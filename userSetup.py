# coding=ascii

"""
!@Brief Maya custom preferences initializer
"""

# =====================================
#	Import Modules
# =====================================

import os
import sys


# =====================================
#	Init variables
# =====================================

kRoot = r'C:\Users\rde\Documents\maya\scripts\IsartDigital'
kToolsDir = 'Tools'
kNodesDir = 'Nodes'
kIconsDir = 'Icons'

kPlugins = 'MAYA_PLUG_IN_PATH'
kIcons = 'XBMLANGPATH'


# =====================================
#	Install tools
# =====================================

sys.path.insert(0, kRoot)


# =====================================
#	Install plugins
# =====================================

for k, v in zip([kPlugins, kIcons], [kNodesDir, kIconsDir]):
	try:
		os.environ[k] = '{0};{1}'.format(os.environ[k], os.path.join(kRoot, v))
	except Exception as e:
		raise RuntimeError('Error on add {0} path !'.format(k))
