# coding=ascii

"""
!@Brief Maya node for procedural operation.
"""

#======================================================================
#   Import Modules
#======================================================================

import math

from maya import OpenMaya, OpenMayaMPx


#======================================================================
#   Math Utils Node
#======================================================================

class RDBasicProcedural(OpenMayaMPx.MPxNode):

    """
    !@Brief Maya custom node.
    """

    #   Static Variables
    kPluginNode = 'rdBasicProcedural'
    kPluginNodeID = OpenMaya.MTypeId(0x1851300)
    kPluginNodeType = OpenMayaMPx.MPxNode.kDependNode

    INPUT_VALUE = OpenMaya.MObject()
    AMPLITUDE = OpenMaya.MObject()
    SPEED = OpenMaya.MObject()
    PICK_POSITION = OpenMaya.MObject()
    PINCH = OpenMaya.MObject()

    OUTPUT_VALUE = OpenMaya.MObject()
    OUTPUT_ANGLE = OpenMaya.MObject()

    #   Node Creator
    @classmethod
    def creator(cls):
        return OpenMayaMPx.asMPxPtr(cls())

    #   Node INIT
    def __init__(self) :
        super(RDBasicProcedural, self).__init__()

    #   Set MFnAttribute
    @staticmethod
    def set_mfn_attribute(mfn_attribute, keyable, readable, writable, storable, connectable):

        mfn_attribute.setKeyable(keyable)
        mfn_attribute.setReadable(readable)
        mfn_attribute.setWritable(writable)
        mfn_attribute.setStorable(storable)
        mfn_attribute.setConnectable(connectable)

    #   Node Initializer
    @classmethod
    def initializer(cls) :

        a_in_attrs = list()
        a_out_attrs = list()

        #   Input attributes
        mfn_input_value_attr = OpenMaya.MFnNumericAttribute()
        cls.INPUT_VALUE = mfn_input_value_attr.create("inputValue", "iv", OpenMaya.MFnNumericData.kDouble, 0.0)
        a_in_attrs.append(cls.INPUT_VALUE)
        cls.set_mfn_attribute(mfn_input_value_attr, True, False, True, True, True)

        mfn_amplitude_attr = OpenMaya.MFnNumericAttribute()
        cls.AMPLITUDE = mfn_amplitude_attr.create("amplitude", "a", OpenMaya.MFnNumericData.kDouble, 1.0)
        a_in_attrs.append(cls.AMPLITUDE)
        cls.set_mfn_attribute(mfn_amplitude_attr, True, False, True, True, True)

        mfn_speed_attr = OpenMaya.MFnNumericAttribute()
        cls.SPEED = mfn_speed_attr.create("speed", "s", OpenMaya.MFnNumericData.kDouble, 30.0)
        a_in_attrs.append(cls.SPEED)
        cls.set_mfn_attribute(mfn_speed_attr, True, False, True, True, True)

        mfn_pick_position_attr = OpenMaya.MFnNumericAttribute()
        cls.PICK_POSITION = mfn_pick_position_attr.create("highPosition", "hp", OpenMaya.MFnNumericData.kDouble, 0.5)
        a_in_attrs.append(cls.PICK_POSITION)
        cls.set_mfn_attribute(mfn_pick_position_attr, True, False, True, True, True)
        mfn_pick_position_attr.setMin(0.0)
        mfn_pick_position_attr.setMax(1.0)

        mfn_pinch_attr = OpenMaya.MFnNumericAttribute()
        cls.PINCH = mfn_pinch_attr.create("pinch", "p", OpenMaya.MFnNumericData.kDouble, 1.0)
        a_in_attrs.append(cls.PINCH)
        cls.set_mfn_attribute(mfn_pinch_attr, True, False, True, True, True)
        mfn_pinch_attr.setMin(0.0)

        #   Output attributes
        mfn_output_value_attr = OpenMaya.MFnUnitAttribute()
        cls.OUTPUT_VALUE = mfn_output_value_attr.create("outputValue", "ov", OpenMaya.MFnUnitAttribute.kDistance, 0.0)
        a_out_attrs.append(cls.OUTPUT_VALUE)
        cls.set_mfn_attribute(mfn_output_value_attr, False, True, False, True, True)

        mfn_output_angle_attr = OpenMaya.MFnUnitAttribute()
        cls.OUTPUT_ANGLE = mfn_output_angle_attr.create("outputAngle", "oa", OpenMaya.MFnUnitAttribute.kAngle, 0.0)
        a_out_attrs.append(cls.OUTPUT_ANGLE)
        cls.set_mfn_attribute(mfn_output_angle_attr, False, True, False, True, True)


        #   Add attributes
        for attribute in (a_in_attrs + a_out_attrs):
            cls.addAttribute(attribute)

        #   Set the attribute dependencies
        for outAttr in a_out_attrs:
            for inAttr in a_in_attrs:
                cls.attributeAffects(inAttr, outAttr)

    #   Node Compute
    def compute(self, plug, data):

        if plug not in [self.OUTPUT_VALUE, self.OUTPUT_ANGLE]:
            return

        #    Get input datas
        f_amplitude = data.inputValue(self.AMPLITUDE).asDouble()
        f_speed = data.inputValue(self.SPEED).asDouble()
        f_input = data.inputValue(self.INPUT_VALUE).asDouble()
        f_pick_pos = data.inputValue(self.PICK_POSITION).asDouble()
        f_pinch = data.inputValue(self.PINCH).asDouble()

        #   Math Sinus
        f_pid2 = math.pi / 2.0
        f_pim2 = 2.0 * math.pi
        f_x_power = 1 + abs(10 * (f_pick_pos - 0.5))
        f_input = f_input % f_speed;
        f_x_raised = f_input / f_speed if f_pick_pos <= 0.5 else 1.0 - (f_input / f_speed)
        f_output = f_amplitude * math.pow(((1.0 + math.sin(-f_pid2 + f_pim2 * math.pow(f_x_raised, f_x_power))) * 0.5), f_pinch)

        #   Set output
        if plug == self.OUTPUT_VALUE:
            output_data = data.outputValue(self.OUTPUT_VALUE)
            output_data.setMDistance(OpenMaya.MDistance(f_output))
        
        if plug == self.OUTPUT_ANGLE:
            output_data = data.outputValue(self.OUTPUT_ANGLE)
            output_data.setMAngle(OpenMaya.MAngle(f_output))

        #   Clean plug
        data.setClean(plug)


# ========================================================
#   Initialize Plugin
# ========================================================

#   load
def initializePlugin(m_object):

    """
    !@Brief Maya initialize node function.
    """

    plugin = OpenMayaMPx.MFnPlugin(m_object, "Remi Deletrain -- remi.deletrain@gmail.com", "1.0", "Any")

    try:
        plugin.registerNode(
            RDBasicProcedural.kPluginNode,
            RDBasicProcedural.kPluginNodeID,
            RDBasicProcedural.creator,
            RDBasicProcedural.initializer,
            RDBasicProcedural.kPluginNodeType)
    except Exception as e:
        raise RuntimeError("Failed to register node: {0}\n\t{1}".format(RDBasicProcedural.kPluginNode, e))


#   Unload
def uninitializePlugin(m_object):

    """
    !@Brief Maya uninitialize node function.
    """

    plugin = OpenMayaMPx.MFnPlugin(m_object)

    try:
        plugin.deregisterNode(RDBasicProcedural.kPluginNodeID)
    except Exception as e:
        raise RuntimeError("Failed to register node: {0}\n\t{1}".format(RDBasicProcedural.kPluginNode, e))