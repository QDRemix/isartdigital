# coding=ascii

"""
!@Brief Maya node for procedural operation.
"""

#======================================================================
#   Import Modules
#======================================================================

import math

from maya import OpenMaya, OpenMayaMPx


#======================================================================
#   Math Utils Node
#======================================================================

class RDBasicProceduralHierarchy(OpenMayaMPx.MPxNode):

    """
    !@Brief Maya custom node.
    """

    #   Static Variables
    kPluginNode = 'rdBasicProceduralHierarchy'
    kPluginNodeID = OpenMaya.MTypeId(0x1851301)
    kPluginNodeType = OpenMayaMPx.MPxNode.kDependNode

    INPUT_VALUE = OpenMaya.MObject()
    INPUT_OFFSET = OpenMaya.MObject()
    OFFSET_MULTIPLICATOR = OpenMaya.MObject()
    AMPLITUDE = OpenMaya.MObject()
    SPEED = OpenMaya.MObject()
    PICK_POSITION = OpenMaya.MObject()
    PINCH = OpenMaya.MObject()

    OUTPUT_VALUE = OpenMaya.MObject()

    #   Node Creator
    @classmethod
    def creator(cls):
        return OpenMayaMPx.asMPxPtr(cls())

    #   Node INIT
    def __init__(self) :
        super(RDBasicProceduralHierarchy, self).__init__()

    #   Set MFnAttribute
    @staticmethod
    def set_mfn_attribute(mfn_attribute, keyable, readable, writable, storable, connectable):

        mfn_attribute.setKeyable(keyable)
        mfn_attribute.setStorable(storable)

    #   Node Initializer
    @classmethod
    def initializer(cls) :

        a_in_attrs = list()
        a_out_attrs = list()

        #   Input attributes
        mfn_input_value_attr = OpenMaya.MFnNumericAttribute()
        cls.INPUT_VALUE = mfn_input_value_attr.create("inputValue", "iv", OpenMaya.MFnNumericData.kDouble, 0.0)
        a_in_attrs.append(cls.INPUT_VALUE)
        mfn_input_value_attr.setKeyable(True)
        mfn_input_value_attr.setStorable(True)

        mfn_input_offset_attr = OpenMaya.MFnNumericAttribute()
        cls.INPUT_OFFSET = mfn_input_offset_attr.create("inputOffset", "io", OpenMaya.MFnNumericData.kDouble, 0.0)
        a_in_attrs.append(cls.INPUT_OFFSET)
        mfn_input_offset_attr.setKeyable(True)
        mfn_input_offset_attr.setStorable(True)

        mfn_offset_multiplicator_attr = OpenMaya.MFnNumericAttribute()
        cls.OFFSET_MULTIPLICATOR = mfn_offset_multiplicator_attr.create("offsetMultiplicator", "om", OpenMaya.MFnNumericData.kDouble, 1.0)
        a_in_attrs.append(cls.OFFSET_MULTIPLICATOR)
        mfn_offset_multiplicator_attr.setKeyable(True)
        mfn_offset_multiplicator_attr.setStorable(True)

        mfn_amplitude_attr = OpenMaya.MFnNumericAttribute()
        cls.AMPLITUDE = mfn_amplitude_attr.create("amplitude", "a", OpenMaya.MFnNumericData.kDouble, 1.0)
        a_in_attrs.append(cls.AMPLITUDE)
        mfn_amplitude_attr.setKeyable(True)
        mfn_amplitude_attr.setStorable(True)

        mfn_speed_attr = OpenMaya.MFnNumericAttribute()
        cls.SPEED = mfn_speed_attr.create("speed", "s", OpenMaya.MFnNumericData.kDouble, 30.0)
        a_in_attrs.append(cls.SPEED)
        mfn_speed_attr.setKeyable(True)
        mfn_speed_attr.setStorable(True)

        mfn_pick_position_attr = OpenMaya.MFnNumericAttribute()
        cls.PICK_POSITION = mfn_pick_position_attr.create("highPosition", "hp", OpenMaya.MFnNumericData.kDouble, 0.5)
        a_in_attrs.append(cls.PICK_POSITION)
        mfn_pick_position_attr.setKeyable(True)
        mfn_pick_position_attr.setStorable(True)
        mfn_pick_position_attr.setMin(0.0)
        mfn_pick_position_attr.setMax(1.0)

        mfn_pinch_attr = OpenMaya.MFnNumericAttribute()
        cls.PINCH = mfn_pinch_attr.create("pinch", "p", OpenMaya.MFnNumericData.kDouble, 1.0)
        a_in_attrs.append(cls.PINCH)
        mfn_pinch_attr.setKeyable(True)
        mfn_pinch_attr.setStorable(True)
        mfn_pinch_attr.setMin(0.0)

        #   Output attributes
        mfn_output_value_attr = OpenMaya.MFnUnitAttribute()
        cls.OUTPUT_VALUE = mfn_output_value_attr.create("outputValue", "ov", OpenMaya.MFnUnitAttribute.kDistance, 0.0)
        a_out_attrs.append(cls.OUTPUT_VALUE)
        mfn_output_value_attr.setKeyable(False)
        mfn_output_value_attr.setStorable(False)
        mfn_output_value_attr.setArray(True)
        mfn_output_value_attr.setUsesArrayDataBuilder(True)


        #   Add attributes
        for attribute in (a_in_attrs + a_out_attrs):
            cls.addAttribute(attribute)

        #   Set the attribute dependencies
        for outAttr in a_out_attrs:
            for inAttr in a_in_attrs:
                cls.attributeAffects(inAttr, outAttr)

    #   Node Compute
    def compute(self, plug, data):

        if plug != self.OUTPUT_VALUE:
            return

        #    Get input data
        f_input_value = data.inputValue(self.INPUT_VALUE).asDouble()
        f_input_offset = data.inputValue(self.INPUT_OFFSET).asDouble()
        f_offset_mult = data.inputValue(self.OFFSET_MULTIPLICATOR).asDouble()
        f_amplitude = data.inputValue(self.AMPLITUDE).asDouble()
        f_speed = data.inputValue(self.SPEED).asDouble()
        f_pick_pos = data.inputValue(self.PICK_POSITION).asDouble()
        f_pinch = data.inputValue(self.PINCH).asDouble()

        #    Get output data
        ha_outputs = data.outputArrayValue(self.OUTPUT_VALUE)
        i_output_count = ha_outputs.elementCount()
        f_offset_step = (1.0 / max((i_output_count - 1), 1)) * f_offset_mult
        hb_outputs = ha_outputs.builder()

        #   Math
        f_pid2 = math.pi / 2.0
        f_pim2 = 2.0 * math.pi
        f_x_power = 1 + abs(10 * (f_pick_pos - 0.5))
        for i in range(i_output_count):
            f_input = (f_input_value + f_input_offset + (i * f_offset_step)) % f_speed;
            f_x_raised = f_input / f_speed if f_pick_pos <= 0.5 else 1.0 - (f_input / f_speed)
            f_output = math.pow(((1.0 + math.sin(-f_pid2 + f_pim2 * math.pow(f_x_raised, f_x_power))) * 0.5), f_pinch)
            h_output = hb_outputs.addElement(i)
            h_output.setMDistance(OpenMaya.MDistance(self.__remap_range(f_output, range_to=(-f_amplitude, f_amplitude))))

        #   Clean plug
        data.setClean(plug)
    
    @staticmethod
    def __remap_range(value, range_from=(0.0, 1.0), range_to=(0.0, 100.0)):
    
        """
        !@Brief Remap a specified value from a range to another one.

        @type value: float
        @param value: Input value.
        @type range_from: list
        @param range_from : Range to remap from (x1, y1).
        @type value: list
        @param range_from: Range to remap to (x2, y2).

        @rtype: float
        @return: New value as a float.
        """

        scale = (float(value) - range_from[0]) / (range_from[1] - range_from[0])

        if range_to[0] < range_to[1]:
            return range_to[0] + scale * (range_to[1] - range_to[0])
        else:
            return range_to[0] - scale * (range_to[1] - range_to[0])


# ========================================================
#   Initialize Plugin
# ========================================================

#   load
def initializePlugin(m_object):

    """
    !@Brief Maya initialize node function.
    """

    plugin = OpenMayaMPx.MFnPlugin(m_object, "Remi Deletrain -- remi.deletrain@gmail.com", "1.0", "Any")

    try:
        plugin.registerNode(
            RDBasicProceduralHierarchy.kPluginNode,
            RDBasicProceduralHierarchy.kPluginNodeID,
            RDBasicProceduralHierarchy.creator,
            RDBasicProceduralHierarchy.initializer,
            RDBasicProceduralHierarchy.kPluginNodeType)
    except Exception as e:
        raise RuntimeError("Failed to register node: {0}\n\t{1}".format(RDBasicProceduralHierarchy.kPluginNode, e))


#   Unload
def uninitializePlugin(m_object):

    """
    !@Brief Maya uninitialize node function.
    """

    plugin = OpenMayaMPx.MFnPlugin(m_object)

    try:
        plugin.deregisterNode(RDBasicProceduralHierarchy.kPluginNodeID)
    except Exception as e:
        raise RuntimeError("Failed to register node: {0}\n\t{1}".format(RDBasicProceduralHierarchy.kPluginNode, e))